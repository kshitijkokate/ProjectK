package mission.mars.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mission.mars.dao.impl.UserDAOImpl;

public class LoginController  extends HttpServlet{
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("LoginController.doPost(is working succesfully)");
		
		HttpSession session = request.getSession(true);
		String username = request.getParameter("email");
		String password = request.getParameter("password");
		
		System.out.println("LoginController.doPost() user name - "+username);
		System.out.println("LoginController.doPost() password - "+password);
		
		UserDAOImpl userValidationObj = new UserDAOImpl();
		
		if(userValidationObj.isValidUser(username, password)) {
			session.setAttribute("isSignIn", true);
			RequestDispatcher rd = request.getRequestDispatcher("about.jsp");//about mars page...
			rd.forward(request, response);
		}else {
			session.setAttribute("isSignIn", false);
			RequestDispatcher rd = request.getRequestDispatcher("signin.jsp");
			request.setAttribute("errormsg", "Please Try Again..!");
			rd.forward(request, response);
		}
		
	}

	

}
