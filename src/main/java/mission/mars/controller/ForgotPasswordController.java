package mission.mars.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mission.mars.dao.impl.UserDAOImpl;

public class ForgotPasswordController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8304584681225380696L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("ForgotPasswordController.doPost()");
		
		String email = request.getParameter("email");
		
		UserDAOImpl userValidationObj = new UserDAOImpl();
		
		if(userValidationObj.isValidUserByEmailId(email)) {
			RequestDispatcher rd = request.getRequestDispatcher("forgot.jsp");
			request.setAttribute("errormsg", "Email Sent Successfully, Please Check InBox / Spam Folder");
			rd.forward(request, response);
		}else {
			RequestDispatcher rd = request.getRequestDispatcher("forgot.jsp");
			request.setAttribute("errormsg", "Please Try Again.. Seems like Email id Wont Valid!");
			rd.forward(request, response);
		}
		
	}
	
	
}
