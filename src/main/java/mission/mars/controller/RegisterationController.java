package mission.mars.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mission.mars.dao.impl.UserDAOImpl;
import mission.mars.email.service.EmailUtil;
import mission.mars.model.Registration;

public class RegisterationController extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("LoginController.doPost(is working succesfully)");
		
		Registration regObj = new Registration();
		regObj.setEmailID(request.getParameter("email"));
		regObj.setFirstname(request.getParameter("fname"));
		regObj.setLastname(request.getParameter("lname"));
		regObj.setPassword(request.getParameter("password"));
		regObj.setCourceId(request.getParameter("course"));
		
		UserDAOImpl userValidationObj = new UserDAOImpl();

		if (userValidationObj.addNewUser(regObj)) {
			try {
				 
				EmailUtil.sendEmail(regObj.getEmailID(), "Thank you for joining with LEARNit! have a happy learning : )",
						EmailUtil.emailBody(regObj.getFirstname(), regObj.getEmailID(), regObj.getPassword()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			RequestDispatcher rd = request.getRequestDispatcher("signin.jsp");
			rd.forward(request, response);
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("signup.jsp");
			request.setAttribute("errormsg", "Please Try Again..!");
			rd.forward(request, response);
		}

	}
 

}
