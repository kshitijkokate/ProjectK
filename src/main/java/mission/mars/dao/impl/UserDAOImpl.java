package mission.mars.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mission.mars.dao.UserDAO;
import mission.mars.email.service.EmailUtil;
import mission.mars.model.Registration;

public class UserDAOImpl implements UserDAO {

	public Boolean isValidUser(String username, String passWord) {

		GetDBConnection dbconnection = new GetDBConnection();
		Statement stmt = null;
		try {
			stmt = dbconnection.getDBConnection().createStatement();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		String sql;
		sql = "SELECT * FROM xploriamars.xploria_mars where Email=\'" + username + "\' and Password=\'" + passWord
				+ "\'";
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if (rs.getString("Email") != null) {
					return true;
				}
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return false;
	}

	public Boolean addNewUser(Registration regObj) {

		GetDBConnection dbconnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO xploriamars.xploria_mars (Email, Fullname, Password) VALUES (?, ?, ?)";

		try {
			dbconnection = new GetDBConnection();
			preparedStatement = dbconnection.getDBConnection().prepareStatement(insertTableSQL);

			preparedStatement.setString(1, regObj.getEmailID());
			preparedStatement.setString(2, regObj.getFirstname() + regObj.getLastname());
			preparedStatement.setString(3, regObj.getPassword());
			preparedStatement.setInt(4, 1);

			preparedStatement.executeUpdate();

			System.out.println("Record is inserted into DBUSER table!");

			return true;
		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}
			}

			if (dbconnection != null) {
				try {
					dbconnection.getDBConnection().close();
				} catch (SQLException e) {

					e.printStackTrace();
				}
			}

		}

		return false;
	}

	public Boolean isValidUserByEmailId(String email) {

		GetDBConnection dbconnection = new GetDBConnection();
		Statement stmt = null;
		try {
			stmt = dbconnection.getDBConnection().createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String sql;
		sql = "SELECT Fullname, Email, Password  FROM xploriamars.xploria_mars where Email=\'" + email + "\'";
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sql);
			rs.next();
			if (rs.getString("Email") != null) {
			
				String username = rs.getString("Fullname");
				String userid = rs.getString("Email");
				String password = rs.getString("Password");
				
				EmailUtil.sendEmail(userid, "Please find your User Details", EmailUtil.emailBody(username, userid, password));
				System.out.println("UserDAOImpl.isValidUserByEmailId() - Email Sent Successfully");
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
