package mission.mars.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import mission.mars.dao.DBConnection;

public class GetDBConnection implements DBConnection {

	public java.sql.Connection getDBConnection() {

		Connection connectionObj = null;
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/xploriamars";
		final String usr = "root";
		final String pwd = "admin";

		Statement stmt = null;

		try {

			try {
				Class.forName(JDBC_DRIVER);
			} catch (ClassNotFoundException e) {

				e.printStackTrace();
			}
			System.out.println("Connecting to database...");
			connectionObj = DriverManager.getConnection(DB_URL, usr, pwd);

			System.out.println("Creating statement...");

		} catch (SQLException se) {

			se.printStackTrace();
		}
		System.out.println("Goodbye!");

		System.out.println("GetDBConnection.getDBConnection() - successfully connected");
		return connectionObj;
	}

}
