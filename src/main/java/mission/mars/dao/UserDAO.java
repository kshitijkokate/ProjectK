package mission.mars.dao;

import mission.mars.model.Registration;

public interface UserDAO {

	public Boolean isValidUser(String username, String passWord);

	public Boolean addNewUser(Registration regObj);
	
}
